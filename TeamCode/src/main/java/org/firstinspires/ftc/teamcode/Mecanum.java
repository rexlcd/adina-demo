package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.VoltageSensor;

@TeleOp(name = "MecanumDemo", group = "LinearOpMode")
public class Mecanum extends LinearOpMode {

    private DcMotor frontLeft;
    private DcMotor frontRight;
    private DcMotor backLeft;
    private DcMotor backRight;
    private DcMotor culisanta;
    private DcMotor brat;
    private DcMotor collector;

    private double motorPower = 0.75;

    @Override
    public void runOpMode() throws InterruptedException {

        backLeft = hardwareMap.dcMotor.get("backLeft");
        backRight = hardwareMap.dcMotor.get("backRight");
        frontLeft = hardwareMap.dcMotor.get("frontLeft");
        frontRight = hardwareMap.dcMotor.get("frontRight");
//      culisanta = hardwareMap.dcMotor.get("culisanta");
//      brat = hardwareMap.dcMotor.get("brat");
//      collector = hardwareMap.dcMotor.get("collector");

        backLeft.setDirection(DcMotor.Direction.REVERSE);
        frontLeft.setDirection(DcMotor.Direction.REVERSE);
//      brat.setDirection(DcMotor.Direction.REVERSE);
//      culisanta.setDirection(DcMotor.Direction.REVERSE);

        telemetry.addData("Init >", "Done");
        telemetry.update();

        waitForStart();
        while (opModeIsActive() && !isStopRequested()) {
            double G1LeftStickY = -gamepad1.left_stick_y;
            double G1LeftStickX = gamepad1.left_stick_x;
            double G1RightStickX = gamepad1.right_stick_x;
            boolean G1RightBumper = gamepad1.right_bumper;
            boolean G1LeftBumper = gamepad1.left_bumper;

            double G2LeftStickY = -gamepad2.left_stick_y;
            double G2RightStickY = -gamepad2.right_stick_y;

            move(G1LeftStickX, G1LeftStickY, G1RightStickX);

//          if(G1RightBumper) collector.setPower(-0.5);
//          else if(G1LeftBumper) collector.setPower(1.0);
//          else collector.setPower(0.0);
//
//          culisanta.setPower(G2LeftStickY);
//          brat.setPower(G2RightStickY);

            failsafe();
        }
    }

    private void move(double x, double y, double rotation) {
        double r = Math.hypot(x, y);
        double robotAngle = Math.atan2(y, x) - Math.PI / 4;

        frontLeft.setPower(motorPower * (r * Math.cos(robotAngle) + rotation));
        frontRight.setPower(motorPower * (r * Math.sin(robotAngle) - rotation));
        backLeft.setPower(motorPower * (r * Math.sin(robotAngle) + rotation));
        backRight.setPower(motorPower * (r * Math.cos(robotAngle) - rotation));
    }

    private void failsafe() {
        if(getBatteryVoltage() < 10) {
            frontRight.setPower(0.0);
            frontLeft.setPower(0.0);
            backRight.setPower(0.0);
            backLeft.setPower(0.0);
            culisanta.setPower(0.0);
            brat.setPower(0.0);
            collector.setPower(0.0);
        }
    }

    private double getBatteryVoltage() {
        double result = Double.POSITIVE_INFINITY;
        for (VoltageSensor sensor : hardwareMap.voltageSensor) {
            double voltage = sensor.getVoltage();
            if (voltage > 0) {
                result = Math.min(result, voltage);
            }
        }
        return result;
    }
}