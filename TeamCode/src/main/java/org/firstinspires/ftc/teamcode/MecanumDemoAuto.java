package org.firstinspires.ftc.teamcode;

import android.util.Log;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cRangeSensor;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.GyroSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.VoltageSensor;
import com.vuforia.CameraDevice;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

@Autonomous(name = "MecanumDemoAuto", group = "LinearOpMode")
public class MecanumDemoAuto extends LinearOpMode {


    private static final String VUFORIA_KEY = "ATD0J3D/////AAABmd39V35PGUhYl+CL7UZFkI95JbUeEL99dL/kF1Zn7cyhPoSCvATY6s1es0/AgsJo7nt818iId/Ugcs8cMHP7kP+LG7ixm7asuVSJqdkcOcJjnBMnIiTRLpMXKLOgbDLFHeCfDUt/C5kVVmFWyK3s8Uch5AnctvE4Z8GJfZlL0um5gQU4jw9Yok4K/+T7gMEc+3yfDqCyH9wzjddLZvt8IOyW9mBu63IKFFFL/wlmZjyFYF+KFqcgoUMEaY9uiC+tBW+P7ihKTyC8vdLBrVXIcigjEp6umkDHnWwnHiVDElwCaolWVc8NEAtOZA3jgCXHuXheh4L1slHm+jJohD1y7CJ0+8N/CXYXnIMCmykZoxiv";
    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    private static final String TAG = "VuforiaTFOD.Test";

    private VuforiaLocalizer localizer;
    private TFObjectDetector detector;

    private DcMotor frontLeft;
    private DcMotor frontRight;
    private DcMotor backLeft;
    private DcMotor backRight;
    private DcMotor culisanta;
    private DcMotor brat;
    private DcMotor collector;
    private ModernRoboticsI2cGyro gyro;
    private Servo servo;
    private ModernRoboticsI2cRangeSensor range;

    DcMotor[] motors;

    private double motorPower = 1.0;

    @Override
    public void runOpMode() throws InterruptedException {

        range = hardwareMap.get(ModernRoboticsI2cRangeSensor.class, "range");
        gyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "gyro");
        servo = hardwareMap.servo.get("servo");
        backLeft = hardwareMap.dcMotor.get("backLeft");
        backRight = hardwareMap.dcMotor.get("backRight");
        frontLeft = hardwareMap.dcMotor.get("frontLeft");
        frontRight = hardwareMap.dcMotor.get("frontRight");
//      culisanta = hardwareMap.dcMotor.get("culisanta");
//      brat = hardwareMap.dcMotor.get("brat");
//      collector = hardwareMap.dcMotor.get("collector");

        motors = new DcMotor[] { frontLeft, frontRight, backLeft, backRight };

        backLeft.setDirection(DcMotor.Direction.REVERSE);
        frontLeft.setDirection(DcMotor.Direction.REVERSE);
        servo.setPosition(0.7);
//      brat.setDirection(DcMotor.Direction.REVERSE);
//      culisanta.setDirection(DcMotor.Direction.REVERSE);

        for(DcMotor m: motors) {
            m.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            Thread.sleep(250);
            m.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        }

        telemetry.addData(">", "Gyro Calibrating. Do Not move!");
        telemetry.update();
        gyro.calibrate();

        while (gyro.isCalibrating())  {
            Thread.sleep(50);
            idle();
        }

        telemetry.addData(">", "Gyro Calibrated.");
        telemetry.update();

        // ---- vuforia initialization
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();
        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        this.localizer = ClassFactory.getInstance().createVuforia(parameters);

        // ---- tfod initialization
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        this.detector = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, this.localizer);
        this.detector.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);

        while(!gamepad1.x) {
            if(gamepad1.x) break;

            telemetry.addData("Heading >", gyro.getHeading());
            telemetry.update();

            idle();
        }

        telemetry.addData(">", "Init done.");
        telemetry.update();

        waitForStart();

        CameraDevice.getInstance().setFlashTorchMode(true);

        if(this.detector != null) {
            this.detector.activate();
        }

        while (opModeIsActive() && !isStopRequested()) {
            double G1LeftStickY = -gamepad1.left_stick_y;
            double G1LeftStickX = gamepad1.left_stick_x;
            double G1RightStickX = gamepad1.right_stick_x;
            boolean G1RightBumper = gamepad1.right_bumper;
            boolean G1LeftBumper = gamepad1.left_bumper;

            double G2LeftStickY = -gamepad2.left_stick_y;
            double G2RightStickY = -gamepad2.right_stick_y;

            move(G1LeftStickX, G1LeftStickY, G1RightStickX);
//
            for(DcMotor m: motors) {
                int pos = m.getCurrentPosition();
                telemetry.addData("motorPos", pos);
            }
            int heading = gyro.getHeading();
            telemetry.addData("heading", heading);
//            telemetry.addData("target", this.target);
//            telemetry.addData("willTurn", this.getTurnDirection(heading, this.target).toString());
            telemetry.update();

            if(gamepad1.a) {
                while(gamepad1.a) { idle(); }
                this.strafeToTarget(1500, 0.4, motors);
                Thread.sleep(1000);
                this.strafeToTarget(-1500, 0.4, motors);
            }

            if(gamepad1.b) {
                while(gamepad1.b) { idle(); }
                this.move(-0.5, 0.0, 0.0, 1000);
            }


            if(gamepad1.y) {
                while (gamepad1.y) idle();
                turnToTarget(113, 0.3);
                Thread.sleep(500);
//                this.move(-0.5, 0.0, 0.0, 1000);
//                this.strafeToTarget(-800, 0.4, motors);
//                Thread.sleep(500);
                moveStraightToTarget(-400, 0.3, motors);
                Thread.sleep(300);

                // do the detection here

                List<Recognition> recognitions = null;
                GoldMineralPosition position = GoldMineralPosition.NONE;
                while(opModeIsActive()) {
                    if(this.detector != null) {
                        recognitions = this.detector.getUpdatedRecognitions();
                        if(recognitions != null && recognitions.size() == 2) {
                            Recognition mineral1 = recognitions.get(0);
                            Recognition mineral2 = recognitions.get(1);

                            if(mineral1.getLeft() < mineral2.getLeft()) {
                                if(mineral1.getLabel() == LABEL_GOLD_MINERAL && mineral2.getLabel() == LABEL_SILVER_MINERAL) {
                                    position = GoldMineralPosition.CENTER;
                                }
                                else if(mineral1.getLabel() == LABEL_SILVER_MINERAL && mineral2.getLabel() == LABEL_GOLD_MINERAL) {
                                    position = GoldMineralPosition.RIGHT;
                                }
                                else if(mineral1.getLabel() == LABEL_SILVER_MINERAL && mineral2.getLabel() == LABEL_SILVER_MINERAL) {
                                    position = GoldMineralPosition.LEFT;
                                }
                            }
                            else if(mineral2.getLeft() < mineral1.getLeft()) {
                                if(mineral1.getLabel() == LABEL_GOLD_MINERAL && mineral2.getLabel() == LABEL_SILVER_MINERAL) {
                                    position = GoldMineralPosition.RIGHT;
                                }
                                else if(mineral1.getLabel() == LABEL_SILVER_MINERAL && mineral2.getLabel() == LABEL_GOLD_MINERAL) {
                                    position = GoldMineralPosition.CENTER;
                                }
                                else if(mineral1.getLabel() == LABEL_SILVER_MINERAL && mineral2.getLabel() == LABEL_SILVER_MINERAL) {
                                    position = GoldMineralPosition.LEFT;
                                }
                            }

                            if(position != GoldMineralPosition.NONE)
                                break;
                        }
                    }
                    idle();
                }

                telemetry.addData("Gold Position", position.toString());
                telemetry.update();

                if(position == GoldMineralPosition.LEFT) {
                    turnToTarget(160, 0.3);
                    moveStraightToTarget(-2200, 0.3, motors);
                }
                else if(position == GoldMineralPosition.CENTER) {
                    turnToTarget(130, 0.3);
                    moveStraightToTarget(-1800, 0.3, motors);
                }
                else if(position == GoldMineralPosition.RIGHT) {
                    turnToTarget(100, 0.3);
                    moveStraightToTarget(-2100, 0.3, motors);
                }

                moveStraightToTarget(1200, 0.4, motors);
                Thread.sleep(300);
                turnToTarget(113, 0.3);
                Thread.sleep(300);
                move(0.5, 0.0, 0.0);
                while(opModeIsActive() && range.getDistance(DistanceUnit.CM) > 40.0) {
                    idle();
                }
                move(0.0, 0.0, 0.0);
                Thread.sleep(300);
                turnToTarget(87, 0.3);
                Thread.sleep(300);

                move(0.5, 0.0, 0.0);
                while(opModeIsActive() && range.getDistance(DistanceUnit.CM) > 18.0) {
                    idle();
                }
                move(0.0, 0.0, 0.0);
                Thread.sleep(300);
                turnToTarget(87, 0.3);
                Thread.sleep(300);

                moveStraightToTarget(4000, 0.4, motors);
                servo.setPosition(0.1);

                Thread.sleep(300);
                turnToTarget(85, 0.3);
                Thread.sleep(300);

                moveStraightToTarget(-6000, 0.3, motors);
                servo.setPosition(0.7);


                Thread.sleep(6000);
                // while(opModeIsActive());
            }

            if(gamepad1.x) {
                while (gamepad1.x) idle();
                turnToTarget(195, 0.3);
                Thread.sleep(500);
//                this.move(-0.5, 0.0, 0.0, 1000);
//                this.strafeToTarget(-800, 0.4, motors);
//                Thread.sleep(500);
                moveStraightToTarget(-350, 0.3, motors);
                Thread.sleep(300);

                // do the detection here

                List<Recognition> recognitions = null;
                GoldMineralPosition position = GoldMineralPosition.NONE;
                while(opModeIsActive()) {
                    if(this.detector != null) {
                        recognitions = this.detector.getUpdatedRecognitions();
                        if(recognitions != null && recognitions.size() == 2) {
                            Recognition mineral1 = recognitions.get(0);
                            Recognition mineral2 = recognitions.get(1);

                            if(mineral1.getLeft() < mineral2.getLeft()) {
                                if(mineral1.getLabel() == LABEL_GOLD_MINERAL && mineral2.getLabel() == LABEL_SILVER_MINERAL) {
                                    position = GoldMineralPosition.CENTER;
                                }
                                else if(mineral1.getLabel() == LABEL_SILVER_MINERAL && mineral2.getLabel() == LABEL_GOLD_MINERAL) {
                                    position = GoldMineralPosition.RIGHT;
                                }
                                else if(mineral1.getLabel() == LABEL_SILVER_MINERAL && mineral2.getLabel() == LABEL_SILVER_MINERAL) {
                                    position = GoldMineralPosition.LEFT;
                                }
                            }
                            else if(mineral2.getLeft() < mineral1.getLeft()) {
                                if(mineral1.getLabel() == LABEL_GOLD_MINERAL && mineral2.getLabel() == LABEL_SILVER_MINERAL) {
                                    position = GoldMineralPosition.RIGHT;
                                }
                                else if(mineral1.getLabel() == LABEL_SILVER_MINERAL && mineral2.getLabel() == LABEL_GOLD_MINERAL) {
                                    position = GoldMineralPosition.CENTER;
                                }
                                else if(mineral1.getLabel() == LABEL_SILVER_MINERAL && mineral2.getLabel() == LABEL_SILVER_MINERAL) {
                                    position = GoldMineralPosition.LEFT;
                                }
                            }

                            if(position != GoldMineralPosition.NONE)
                                break;
                        }
                    }
                    idle();
                }

                telemetry.addData("Gold Position", position.toString());
                telemetry.update();

                if(position == GoldMineralPosition.LEFT) {
                    turnToTarget(247, 0.3);
                    moveStraightToTarget(-2200, 0.3, motors);
                }
                else if(position == GoldMineralPosition.CENTER) {
                    turnToTarget(215, 0.3);
                    moveStraightToTarget(-1800, 0.3, motors);
                }
                else if(position == GoldMineralPosition.RIGHT) {
                    turnToTarget(185, 0.3);
                    moveStraightToTarget(-2100, 0.3, motors);
                }

                moveStraightToTarget(1300, 0.4, motors);
                Thread.sleep(300);
                turnToTarget(43, 0.3);
                Thread.sleep(300);
                move(0.5, 0.0, 0.0);
                while(opModeIsActive() && range.getDistance(DistanceUnit.CM) > 40.0) {
                    idle();
                }
                move(0.0, 0.0, 0.0);
                Thread.sleep(300);
                turnToTarget(87, 0.3);
                Thread.sleep(300);

                move(0.5, 0.0, 0.0);
                while(opModeIsActive() && range.getDistance(DistanceUnit.CM) > 18.0) {
                    idle();
                }
                move(0.0, 0.0, 0.0);
                Thread.sleep(300);
                turnToTarget(87, 0.3);
                Thread.sleep(300);

                moveStraightToTarget(4000, 0.4, motors);
                servo.setPosition(0.1);

                Thread.sleep(300);
                turnToTarget(85, 0.3);
                Thread.sleep(300);

                moveStraightToTarget(-6000, 0.3, motors);
                servo.setPosition(0.7);


                Thread.sleep(6000);
                // while(opModeIsActive());
            }

//            if(G1RightBumper) collector.setPower(-0.5);
//            else if(G1LeftBumper) collector.setPower(1.0);
//            else collector.setPower(0.0);
//
//            culisanta.setPower(G2LeftStickY);
//            brat.setPower(G2RightStickY);

            // failsafe();
        }

        CameraDevice.getInstance().setFlashTorchMode(false);
    }

    private enum ETurnDirection {
        LEFT,
        RIGHT
    }

    private enum GoldMineralPosition {
        NONE,
        LEFT,
        CENTER,
        RIGHT
    }

    private ETurnDirection getTurnDirection(int current, int target) {
        return current >= target ?
                (Math.abs(current - target) > 180 ? ETurnDirection.LEFT : ETurnDirection.RIGHT) :
                (Math.abs(current - target) > 180 ? ETurnDirection.RIGHT : ETurnDirection.LEFT);
    }

    private int opposite(int angle) {
        return (angle + 180) % 360;
    }

    private void turnToTarget(int target, double power) throws InterruptedException {
        if(getTurnDirection(gyro.getHeading(), target) == ETurnDirection.RIGHT) {
            move(0.0, 0.0, power);
        }
        else {
            move(0.0, 0.0, -power);
        }
        int lower = Math.max(target - 2, 0);
        int higher = Math.min(target + 2, 359);

        int heading = gyro.getHeading();
        while(opModeIsActive()) { idle(); /*telemetry.addData("heading", heading); telemetry.update(); */ heading = gyro.getHeading(); if(heading >= lower && heading <= higher) break; }

        move(0.0, 0.0, 0.0);
        Thread.sleep(100);
    }

    private void moveStraightToTarget(int target, double speed, DcMotor[] motors) throws InterruptedException {
        for(DcMotor motor: motors) {
            motor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            Thread.sleep(20);
        }

        for(DcMotor motor: motors) {
            motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            motor.setTargetPosition(target);
            motor.setPower(speed);
        }

        while (motors[0].isBusy()) {
            idle();
            telemetry.addData("motor", motors[0].getCurrentPosition());
            telemetry.update();
        }

        for(DcMotor motor: motors) {
            motor.setPower(0.0);
            motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        }
    }

    private void strafeToTarget(int target, double speed, DcMotor[] motors) throws InterruptedException {
        for(DcMotor motor: motors) {
            motor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            Thread.sleep(20);
        }

        frontLeft.setTargetPosition(target);
        backLeft.setTargetPosition(-target);
        frontRight.setTargetPosition(-target);
        backRight.setTargetPosition(target);

        for(DcMotor motor: motors) {
            motor.setPower(speed);
            motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        }

        while (motors[0].isBusy()) {
            idle();
            telemetry.addData("motor", motors[0].getCurrentPosition());
            telemetry.update();
        }

        for(DcMotor motor: motors) {
            motor.setPower(0.0);
            motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        }
    }

    private void diagRightToTarget(int target, double speed, DcMotor[] motors) throws InterruptedException {
        for(DcMotor motor: motors) {
            motor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            Thread.sleep(20);
        }

        if(target > 0) {
            frontLeft.setTargetPosition(target);
            backRight.setTargetPosition(target);
        }
        else {
            frontRight.setTargetPosition(-target);
            backLeft.setTargetPosition(-target);
        }

        for(DcMotor motor: motors) {
            motor.setPower(speed);
            motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        }

        while (motors[0].isBusy()) {
            idle();
            telemetry.addData("motor", motors[0].getCurrentPosition());
            telemetry.update();
        }

        for(DcMotor motor: motors) {
            motor.setPower(0.0);
            motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        }
    }

    private void diagLeftToTarget(int target, double speed, DcMotor[] motors) throws InterruptedException {
        for(DcMotor motor: motors) {
            motor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            Thread.sleep(20);
        }

        if(target > 0) {
            frontRight.setTargetPosition(target);
            backLeft.setTargetPosition(target);
        }
        else {
            frontLeft.setTargetPosition(-target);
            backRight.setTargetPosition(-target);
        }

        for(DcMotor motor: motors) {
            motor.setPower(speed);
            motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        }

        while (motors[0].isBusy()) {
            idle();
            telemetry.addData("motor", motors[0].getCurrentPosition());
            telemetry.update();
        }

        for(DcMotor motor: motors) {
            motor.setPower(0.0);
            motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        }
    }

    private void move(double left, double right, double rotation, long time) throws InterruptedException {
        move(left, right, rotation);
        Thread.sleep(time);
        move(0.0, 0.0, 0.0);
    }

    private void move(double x, double y, double rotation) {
        double r = Math.hypot(x, y);
        double robotAngle = Math.atan2(y, x) - Math.PI / 4;

        frontLeft.setPower(motorPower * (r * Math.cos(robotAngle) + rotation));
        frontRight.setPower(motorPower * (r * Math.sin(robotAngle) - rotation));
        backLeft.setPower(motorPower * (r * Math.sin(robotAngle) + rotation));
        backRight.setPower(motorPower * (r * Math.cos(robotAngle) - rotation));
    }

    private void failsafe() {
        if(getBatteryVoltage() < 10.5) {
            frontRight.setPower(0.0);
            frontLeft.setPower(0.0);
            backRight.setPower(0.0);
            backLeft.setPower(0.0);
//            culisanta.setPower(0.0);
//            brat.setPower(0.0);
//            collector.setPower(0.0);
        }
    }

    public double getBatteryVoltage() {
        double result = Double.POSITIVE_INFINITY;
        for (VoltageSensor sensor : hardwareMap.voltageSensor) {
            double voltage = sensor.getVoltage();
            if (voltage > 0) {
                result = Math.min(result, voltage);
            }
        }
        return result;
    }
}